import inquirer from "inquirer";
import { axiosInstance, hello } from ".";
import { getPostsForUser } from "./post";

export type User = {
  name: string;
  id: number;
};

export async function getUser(usrName: string): Promise<User | undefined> {
  const uN = usrName.toLowerCase();
  let usr;
  const {
    data: [user],
  } = await axiosInstance.get(`/users/?name=${uN}`);
  if (!user) {
    console.log("User not found");
    const { createUser } = await inquirer.prompt({
      name: "createUser",
      prefix: "",
      message: "Create user?",
      type: "confirm",
    });

    if (createUser === true) {
      try {
        const { data } = await axiosInstance.post(
          "/users",
          JSON.stringify({ name: uN })
        );
        usr = data;
      } catch (e) {
        console.log(e);
      }
    }
    if (createUser === false) {
      return;
    }
  }

  if (!usr) {
    usr = user;
  }

  return usr;
}

export async function readUser(usrName: string) {
  const user = await getUser(usrName);
  if (!user) {
    return hello();
  }
  const postsForUser = await getPostsForUser(user.id);
  if (postsForUser.length) {
    console.log(postsForUser.join("\n"));
  } else {
    console.log("No posts for this user.");
  }

  return hello();
}

export async function followUser(follower: string, followee: string) {
  const actingUser = await getUser(follower);
  const userBeingActedOn = await getUser(followee);

  if (!actingUser || !userBeingActedOn) {
    return hello();
  }

  console.log(`${follower} follows ${followee}`);

  return hello();
}
