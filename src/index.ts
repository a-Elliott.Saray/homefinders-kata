#!/user/bin/env node

import { clear } from "console";
import inquirer from "inquirer";
import { parseCmd } from "./parse";

import axios from "axios";

export const axiosInstance = axios.create({
  baseURL: "http://localhost:3000",
  headers: {
    "Content-Type": "application/json",
  },
});

export async function hello() {
  const { helloCmd } = await inquirer.prompt({
    type: "input",
    name: "helloCmd",
    prefix: "",
    message: ">",
  });
  parseCmd(helloCmd);
}

clear();
hello();
