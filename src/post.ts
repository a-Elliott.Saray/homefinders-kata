import { formatDistance, isBefore, parseISO } from "date-fns";
import { axiosInstance, hello } from ".";
import { getUser } from "./user";

export type Post = {
  message: string;
  time: string;
  userId: number;
};

export async function postUserMessage(userName: string, message: string) {
  const user = await getUser(userName);
  if (user) {
    await axiosInstance.post(
      "/posts",
      JSON.stringify({
        message,
        userId: user.id,
        time: new Date().toISOString(),
      })
    );
  }
  return hello();
}

export async function getPostsForUser(userId: number): Promise<string[]> {
  const { data: posts } = await axiosInstance.get<Post[]>(
    `/posts?userId=${userId}`
  );

  if (!posts.length) {
    return [];
  }

  return posts
    .sort(({ time: timeA }, { time: timeB }) =>
      isBefore(parseISO(timeA), parseISO(timeB)) ? 1 : -1
    )
    .map(
      ({ message, time }) =>
        `${message} (${formatDistance(parseISO(time), new Date())} ago)`
    );
}
