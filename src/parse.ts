import { hello } from ".";
import { postUserMessage } from "./post";
import { followUser, readUser } from "./user";

type RegExCommand = "->" | "follows" | "wall" | "likes" | "unlike";

const userRegEx = /^(?<user>[^\s]+)/gi;
const userReadRegEx = /^(?<user>[^\s]+)$/gi;
const actionRegEx =
  /^(?<user>[^\s]+)(\s)?(?<command>((->)|(follows)|(wall)|(likes)|(unlike)))(\s)?(?<message>.+)?$/gi;

export function parseCmd(cmd: string) {
  if (!cmd.match(userRegEx)) {
    console.log("Please use your name to begin your action.");
    return hello();
  }

  if (cmd.match(userReadRegEx)) {
    return readUser(cmd);
  }

  const matches = actionRegEx.exec(cmd);

  if (matches) {
    const { groups } = matches;
    if (groups?.command) {
      const { command, user, message } = groups;
      const action = command as RegExCommand;
      if (action === "->") {
        return postUserMessage(groups.user, groups.message);
      }
      if (action === "follows") {
        return followUser(groups.user, groups.message);
      }
    }
  }

  return hello();
}
